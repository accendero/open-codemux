Code Multiplex
---
1. Setup
2. Script Uploading
3. Run Script
4. TODO

Setup
---
Run `docker-compose -f <composer file> up --build` to setup db and app services.

Script Uploading
---
Browse to `http://localhost:8090/scripts/` and post a script file.
* Text and JSON output are identical currently
* File + JSON will return file data with result data
* All File output types expect `arg[0]` of your command to be the filename you wish to create

Run Script
---
Send a JSON payload to your script endpoint (e.g. `/scripts/1/run/`) containing an arg list. Examples of this can be found in the `codemux/post-command.http` file.

 TODO
 ---
 * Allow list of packages to be installed for uploaded scripts
 * Auto determine packages to install on script upload
 * Use content types in response
 