from django.conf import settings
from django.contrib.auth.models import User
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        User.objects.filter(username=settings.APP_SUPERUSER_NAME) or \
            User.objects.create_superuser(settings.APP_SUPERUSER_NAME,
                                          settings.APP_SUPERUSER_EMAIL,
                                          settings.APP_SUPERUSER_PASSWORD)
