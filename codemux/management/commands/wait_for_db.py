import time

from django.core.management import BaseCommand
from django.db import connection, OperationalError


class Command(BaseCommand):
    def handle(self, *args, **options):
        retries = 300
        done = False
        self.stdout.write("waiting for database...")
        while not done and retries:
            try:
                connection.ensure_connection()
                done = True
            except OperationalError:
                self.stdout.write("database not available, waiting...")
                time.sleep(1)
                retries -= 1

        if done:
            self.stdout.write(self.style.SUCCESS("database is online"))
        else:
            raise OperationalError("database not available, max retries reached")
