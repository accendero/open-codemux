from django.conf import settings
from django.db import models

from codemux import choices


class Script(models.Model):
    name = models.CharField(max_length=255)
    file = models.FileField(upload_to=settings.SCRIPT_DIRECTORY)
    output_type = models.CharField(
        choices=choices.OUTPUT_TYPES,
        default=choices.OUTPUT_TYPE_STANDARD,
        max_length=1
    )
    output_content_type = models.CharField(
        choices=choices.CONTENT_TYPES,
        max_length=255
    )
    packages = models.CharField(max_length=255, blank=True, null=True)
    packages_installed = models.CharField(
        max_length=1,
        choices=choices.PACKAGE_INSTALL_CHOICES,
        default=choices.PACKAGE_INSTALL_NONE,
    )
