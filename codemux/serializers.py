from rest_framework import serializers

from codemux import models


class ScriptSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Script
        fields = '__all__'


class ScriptRunSerializer(serializers.ModelSerializer):
    args = serializers.ListField(child=serializers.CharField())

    class Meta:
        model = models.Script
        fields = ('args',)
