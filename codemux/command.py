COMMAND_STRING = {
    'codemux_python': 'python {script_name} {script_args}',
}

ARBITRARY_COMMAND_STRING = {
    'codemux_python': 'python -c "{}"',
}

ARG_DELIMITER = {
    'codemux_python': ' ',
}


# TODO: add package installation for python


INSTALL_PACKAGE_STRING = {
}

INSTALL_PACKAGE_SUCCESS = {
}

INSTALLED_PACKAGES_STRING = {
}

INSTALLED_PACKAGES_PARSE = {
}

EXIT_CODE_ERROR = 0x01
EXIT_CODE_EMBEDDED_FILE = 0xEF
EXIT_CODE_FILE = 0xFF
