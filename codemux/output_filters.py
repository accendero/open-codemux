def filter_python_output(command_output):
    return "{}".format(command_output)


def filter_python_filename(result, _):
    parts = result.split('"')
    path = parts[1]
    return path


def filter_python_import(_):
    return None


OUTPUT_FILTERS = {
    'codemux_python': filter_python_output,
}

FILENAME_FILTERS = {
    'codemux_python': filter_python_filename,
}

IMPORT_FILTERS = {
    'codemux_python': filter_python_import,
}
