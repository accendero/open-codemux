import base64
import logging
import os

from django.conf import settings
from django.http import HttpResponse
from rest_framework import status, viewsets
from rest_framework.decorators import action, api_view
from rest_framework.response import Response

from codemux import models, serializers, choices
from codemux.command import (
    COMMAND_STRING,
    ARG_DELIMITER,
    ARBITRARY_COMMAND_STRING,
    INSTALL_PACKAGE_STRING,
    INSTALL_PACKAGE_SUCCESS,
    INSTALLED_PACKAGES_STRING,
    INSTALLED_PACKAGES_PARSE,
)
from codemux.output_filters import (
    OUTPUT_FILTERS,
    FILENAME_FILTERS,
    IMPORT_FILTERS,
)

logger = logging.Logger(__name__)
BACKEND = os.environ.get('APP_NAME')


def get_command(script, args):
    try:
        if script.output_type == choices.OUTPUT_TYPE_FILE:
            args[0] = os.path.join(
                settings.MEDIA_ROOT,
                args[0]
            )

        command = COMMAND_STRING[BACKEND].format(
            script_name=script.file.path,
            script_args=ARG_DELIMITER[BACKEND].join(args)
        )
    except KeyError:
        raise Exception("no command string set for {}".format(BACKEND))

    if not command:
        raise Exception("no command found for {}, exiting".format(BACKEND))

    return command


def _result(result):
    if OUTPUT_FILTERS.get(BACKEND, None):
        result = OUTPUT_FILTERS[BACKEND](result)
    return result


def run_command(command):
    result = os.popen(command).read()
    return _result(result)


def run_arbitrary_command(command: str) -> str:
    try:
        command = ARBITRARY_COMMAND_STRING[BACKEND].format(command)
        result = os.popen(command).read()
        return _result(result)
    except Exception as e:
        logger.error(e)
        return ''


def get_filename(result, args):
    if FILENAME_FILTERS.get(BACKEND, None):
        return FILENAME_FILTERS[BACKEND](result, args)
    return result


def find_packages(serializer: serializers.ScriptSerializer) -> list:
    obj = serializer.instance
    import_filter = IMPORT_FILTERS.get(BACKEND, None)
    if not import_filter:
        return []
    try:
        contents = obj.file.read().decode()
        packages = import_filter(contents)
        return packages
    except Exception as e:
        print(e)
        raise Exception from e


def has_auto_install() -> bool:
    return all([
        INSTALL_PACKAGE_STRING.get(BACKEND, None) is not None,
        settings.AUTO_INSTALL_PACKAGES
    ])


def get_installed_packages() -> list:
    command = INSTALLED_PACKAGES_STRING.get(BACKEND)
    res = run_arbitrary_command(command)
    return INSTALLED_PACKAGES_PARSE.get(BACKEND)(res)


# TODO: this should schedule packages for installation in a queue
def install_packages(packages: set) -> bool:
    install = INSTALL_PACKAGE_STRING.get(BACKEND, None)
    success = INSTALL_PACKAGE_SUCCESS.get(BACKEND, None)
    results = [run_arbitrary_command(install(p)) for p in packages]
    return all(success(r) for r in results)


@api_view(http_method_names=['get', ])
def installed_packages(request):
    return Response({'packages': get_installed_packages()})


class ScriptViewSet(viewsets.ModelViewSet):
    queryset = models.Script.objects.all()
    serializer_classes = {
        'default': serializers.ScriptSerializer,
        'run': serializers.ScriptRunSerializer,
    }

    def get_serializer_class(self):
        return self.serializer_classes.get(
            self.action, self.serializer_classes['default'])

    def perform_create(self, serializer):
        super().perform_create(serializer)
        if has_auto_install():
            installed = set(get_installed_packages())
            packages = set(find_packages(serializer))
            to_install = packages - installed
            script = serializer.instance
            script.packages = ','.join(packages)
            if not to_install or install_packages(to_install):
                script.packages_installed = choices.PACKAGE_INSTALL_SUCCESS
            else:
                logger.error(
                    f"Could not install packages: {', '.join(packages)}"
                )
                script.packages_installed = choices.PACKAGE_INSTALL_FAIL
            script.save()

    @action(detail=True, methods=['get', 'post'])
    def run(self, request, pk=None):
        script = self.get_object()
        serializer = serializers.ScriptRunSerializer(data=request.data)
        if serializer.is_valid():
            args = serializer.data.get('args', [])
            command = get_command(script, serializer.data.get('args', args))
            result = run_command(command)
            if script.output_type in (
                    choices.OUTPUT_TYPE_STANDARD,
                    choices.OUTPUT_TYPE_JSON,
            ):
                return Response(
                    {
                        'result': result,
                        'command': command
                    },
                    status=status.HTTP_200_OK
                )
            elif script.output_type in (
                    choices.OUTPUT_TYPE_FILE,
                    choices.OUTPUT_TYPE_FILE_AND_JSON,
            ):
                filename = args[0]
                disposition = 'attachment; filename="{}"'.format(filename)
                try:
                    with open(filename, 'rb') as file:
                        data = file.read()
                        if script.output_type == choices.OUTPUT_TYPE_FILE:
                            response = HttpResponse(
                                data, script.output_content_type
                            )
                            response['Content-Disposition'] = disposition
                            return response
                        else:
                            return Response(
                                {
                                    'command': command,
                                    'data': base64.b64encode(data),
                                    'result': result,
                                },
                                status=status.HTTP_200_OK,
                            )
                except Exception as e:
                    logger.error(e)
                    error = 'could not find {}'.format(filename)
                    return Response(
                        {
                            'error': error,
                            'exception': str(e),
                        },
                        status=status.HTTP_400_BAD_REQUEST
                    )
            else:
                return Response(
                    {
                        'error': 'invalid response type',
                        'command': command
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )
