OUTPUT_TYPE_STANDARD = 'S'
OUTPUT_TYPE_FILE = 'F'
OUTPUT_TYPE_JSON = 'J'
OUTPUT_TYPE_FILE_AND_JSON = 'H'
OUTPUT_TYPES = (
    (OUTPUT_TYPE_STANDARD, 'Standard Output'),
    (OUTPUT_TYPE_FILE, 'File Output'),
    (OUTPUT_TYPE_JSON, 'JSON Output'),
    (OUTPUT_TYPE_FILE_AND_JSON, 'JSON and File Output'),
)

CONTENT_TYPES = (
    ('text/csv', 'text/csv'),
    ('image/png', 'image/png'),
    ('application/json', 'application/json'),
)

PACKAGE_INSTALL_NONE = 'N'
PACKAGE_INSTALL_FAIL = 'F'
PACKAGE_INSTALL_SUCCESS = 'S'
PACKAGE_INSTALL_CHOICES = (
    (PACKAGE_INSTALL_NONE, 'None'),
    (PACKAGE_INSTALL_FAIL, 'Failed'),
    (PACKAGE_INSTALL_SUCCESS, 'Success'),
)
