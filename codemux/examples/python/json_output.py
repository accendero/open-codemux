import json

data = [
    {
        'name': 'John',
        'age': 21,
    },
    {
        'name': 'Dora',
        'age': 15,
    }
]

print(json.dumps(data))
