import base64
import json
import os

import requests

from codemux import choices


class Response:
    def __init__(self, response):
        # TODO: Encapsulate error response codes
        self._response = response
        self.response = response.json()
        self.status_code = response.status_code

    def _data(self):
        return self.response.get('data')

    def _result(self):
        return self.response.get('result')

    @property
    def data(self):
        return base64.b64decode(self._data())

    @property
    def result(self):
        return json.loads(self._result())

    def json(self):
        return self.response


class CodemuxWrapper:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.base_url = 'http://{}:{}/'.format(host, port)
        self.scripts_url = '{}scripts/'.format(self.base_url)

    def upload(self, name, path, output_type, accepted_type):
        with open(path) as file:
            data = {
                'name': name,
                'output_type': output_type,
                'output_content_type': accepted_type,
            }
            files = {
                'file': file,
            }
            response = requests.post(
                self.scripts_url,
                data=data,
                files=files
            )
            return Response(response)

    def run(self, script_id, *args):
        url = '{}{}/run/'.format(self.scripts_url, str(script_id))
        response = requests.post(url, json={'args': args})
        return Response(response)


if __name__ == '__main__':
    cm = CodemuxWrapper('localhost', '8080')
    upload_path = os.path.dirname(__file__)
    upload_file = os.path.join(upload_path, 'python', 'json_output.py')
    upload_response = cm.upload(
        'my_script_filename.py',
        upload_file,
        choices.OUTPUT_TYPE_JSON,
        'application/json'
    )
    response_json = upload_response.json()
    print(response_json)

    script_args = []
    run_response = cm.run(response_json.get('id'), *script_args)
    print(run_response.json())
    print(run_response.result)
    print(run_response.data)
